import os
import sys
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from mwconstants.links import link_to_namespace
from mwconstants.namespaces import prefixes_to_canonical