from context import link_to_namespace, prefixes_to_canonical

def test_local():
    lang = 'fr'
    link_text = 'Média:Filename'
    assert link_to_namespace(link_text, lang=lang) == 'Media'

def test_canonical():
    lang = 'fr'
    link_text = 'Project:Article name'
    assert link_to_namespace(link_text, lang=lang) == 'Project'

def test_article():
    lang = 'en'
    link_text = 'Main Page'
    assert link_to_namespace(link_text, lang=lang) is None

def test_alias():
    lang = 'en'
    link_text = 'WP:Article name'
    assert link_to_namespace(link_text, lang=lang) == 'Project'

def test_interwiki():
    lang = 'en'
    link_text = 'en:WP:Article name'
    assert link_to_namespace(link_text, lang=lang) == 'Interlanguage'

def test_precalculated_map():
    prefix_map = prefixes_to_canonical('en')
    lang = 'en'
    link_text = 'WP:Article name'
    assert link_to_namespace(link_text, lang=lang, prefix_map=prefix_map) == 'Project'

def test_improper_style():
    lang = 'fr'
    link_text = 'PROJECT:Article name'
    assert link_to_namespace(link_text, lang=lang) == 'Project'
